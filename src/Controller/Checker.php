<?php

namespace App\Controller;

class Checker {
    
    private function getHalfWord($word) {
        if (empty($word)) {
            return ['', ''];
        }

        return [
            substr( $word, 0, floor(strlen($word)/2) ), 
            substr( $word, floor(strlen($word)/2) )
            ];
    }

    public function isPalindrome($word) {
        if (empty($word)) {
            return false;
        }

        list($firstHalf, $secondHalf) = $this->getHalfWord($word);
        
        if (strlen($word) % 2 === 0) {
            if ( strtolower($firstHalf) == strrev(strtolower($secondHalf)) ) {
                return true;
            }
        } else {
            $secondHalf = substr($secondHalf, 1);
            if ( strtolower($firstHalf) == strrev(strtolower($secondHalf)) ) {
                return true;
            }
        }
        
        return false;
    }
    
    private function removeSpaces($str) {
        return preg_replace('/ /', '', $str);
    }
    
    private function removeSpecialChars($str) {
        return preg_replace('/[^A-Za-z]/', '', $str);
    }

    public function isAnagram($word, $comparison) {
        $word = strtolower($this->removeSpaces($word));
        $comparison = strtolower($this->removeSpaces($comparison));

        if (empty($word) || empty($comparison) || strlen($word) != strlen($comparison)) {
            return false;
        }

        for ($i = 0; $i < strlen($comparison); $i++) {
            if (str_contains($word, $comparison[$i])) {
                $word = preg_replace('/'.$comparison[$i].'/', "", $word, 1);
            } else {
                return false;
            }
        }

        return true;
    }
    
    public function isPangram($phrase) {
        if (empty($phrase)) {
            return false;
        }

        $phrase = strtolower($this->removeSpecialChars($phrase));

        $uniquePhrase = count_chars($phrase, 3);
        
        return $this->isAnagram($uniquePhrase, "abcdefghijklmnopqrstuvwxyz");
    }
}
