<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class CheckerController {
    public function isPalindrome($word): Response
    {
        $checker = new Checker();

        return new Response(
          '<html><body>"'.$word.'" is palindrome: '.($checker->isPalindrome($word)?"true":"false").'</body></html>'
        );
    }
    
    public function isAnagram($word, $comparison): Response
    {
        $checker = new Checker();

        return new Response(
          '<html><body>"'.$word.'" is anagram of "'.$comparison.'": '.($checker->isAnagram($word, $comparison)?"true":"false").'</body></html>'
        );
    }
    
    public function isPangram($phrase): Response
    {
        $checker = new Checker();

        return new Response(
          '<html><body>"'.$phrase.'" is pangram: '.($checker->isPangram($phrase)?"true":"false").'</body></html>'
        );
    }
}
