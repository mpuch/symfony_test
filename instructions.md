- Command for the console terminal to start the local web server is:

	symfony server:start

Server should be available under http://localhost:8000/ .

- Project has three addresses that are availble to use:

	/palindrome/{word}
	/anagram/{word}/{comparison}
	/pangram/{phrase}
	
Parameters in the curly brackets '{}' are the parameters of the adequates methods.

- Installing PHPUnit testing framework:
	composer require --dev phpunit/phpunit symfony/test-pack
	
- Command to run the tests:
	php ./vendor/bin/phpunit tests/Controller/CheckerTest.php