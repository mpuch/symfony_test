<?php declare(strict_types=1);

namespace App\Tests\Controller;

use PHPUnit\Framework\TestCase;
use App\Controller\Checker;

final class CheckerTest extends TestCase{

    public function testCheckers(): void
    {
        $checker = new Checker();
        
        $this->assertTrue( $checker->isPalindrome("Anna") );
        $this->assertFalse( $checker->isPalindrome("Bark") );
        
        $this->assertTrue( $checker->isAnagram("coalface", "cacao elf" ) );
        $this->assertFalse( $checker->isAnagram("coalface", "dark elf" ) );
        $this->assertTrue( $checker->isPangram("The quick brown fox jumps over the lazy dog") );
        $this->assertFalse( $checker->isPangram("The British Broadcasting Corporation (BBC) is a British public service broadcaster.") );
    }
}